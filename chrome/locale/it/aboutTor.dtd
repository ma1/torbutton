<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Info su Tor">

<!ENTITY aboutTor.viewChangelog.label "Vedi il Changelog">

<!ENTITY aboutTor.ready.label "Naviga. Privatamente.">
<!ENTITY aboutTor.ready2.label "Sei pronto per l'esperienza di navigazione più privata al mondo.">
<!ENTITY aboutTor.failure.label "Qualcosa è Andato Storto!">
<!ENTITY aboutTor.failure2.label "Tor non sta funzionando su questo browser.">

<!ENTITY aboutTor.search.label "Cerca con DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Domande?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Controlla il Manuale del Browser Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Manuale Browser Tor">

<!ENTITY aboutTor.tor_mission.label "Il Tor Project è una organizzazione non-profit allo scopo di avanzare i diritti e le libertà umane creando e distribuendo software libero con tecnologie per la privacy e l'anonimato, supportando la loro disponibilità e utilizzo senza restrizioni e avanzandone la loro comprensione scientifica e popolare.">
<!ENTITY aboutTor.getInvolved.label "Unisciti a noi »">

<!ENTITY aboutTor.newsletter.tagline "Ottieni le ultime info da Tor direttamente nella tua casella di posta elettronica.">
<!ENTITY aboutTor.newsletter.link_text "Registrati alle Tor News.">
<!ENTITY aboutTor.donationBanner.freeToUse "L'utilizzo di Tor è gratuito grazie alle donazioni fatte da persone come te.">
<!ENTITY aboutTor.donationBanner.buttonA "Dona Adesso">

<!ENTITY aboutTor.alpha.ready.label "Test. Accuratamente.">
<!ENTITY aboutTor.alpha.ready2.label "Sei pronto per testare l'esperienza di navigazione più privata al mondo.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha è una versione instabile di Tor Browser che puoi usare per provare nuove funzioni, testare le loro prestazioni e fornire feedback prima della pubblicazione.">
<!ENTITY aboutTor.alpha.bannerLink "Segnala un errore nel forum di Tor">

<!ENTITY aboutTor.nightly.ready.label "Test. Accuratamente.">
<!ENTITY aboutTor.nightly.ready2.label "Sei pronto per testare l'esperienza di navigazione più privata al mondo.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly è una versione instabile di Tor Browser che puoi usare per provare nuove funzioni, testare le loro prestazioni e fornire feedback prima della pubblicazione.">
<!ENTITY aboutTor.nightly.bannerLink "Segnala un errore nel forum di Tor">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "ALIMENTATO DALLA PRIVACY">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTENZA">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CAMBIAMENTO">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "LIBERTÀ">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "DONA ORA">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "La tua donazione sarà mandata a Friends of Tor, fino a $100,000.">
