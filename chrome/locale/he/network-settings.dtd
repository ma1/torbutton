<!ENTITY torsettings.dialog.title "הגדרות רשת Tor">
<!ENTITY torsettings.wizard.title.default "התחבר אל Tor">
<!ENTITY torsettings.wizard.title.configure "הגדרות רשת Tor">
<!ENTITY torsettings.wizard.title.connecting "מקים חיבור">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "שפת דפדפן Tor">
<!ENTITY torlauncher.localePicker.prompt "אנא בחר שפה.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "לחץ על “התחבר“ כדי להתחבר אל Tor.">
<!ENTITY torSettings.configurePrompt "לחץ על ”תצר“ כדי להתאים הגדרות רשת אם אתה במדינה שמצנזרת את Tor (למשל מצרים, סין וטורקיה) או אם אתה מתחבר מרשת פרטית הדורשת ייפוי־כוח.">
<!ENTITY torSettings.configure "תצר">
<!ENTITY torSettings.connect "התחבר">

<!-- Other: -->

<!ENTITY torsettings.startingTor "ממתין אל Tor שיתחיל…">
<!ENTITY torsettings.restartTor "הפעל מחדש את Tor">
<!ENTITY torsettings.reconfigTor "תצר מחדש">

<!ENTITY torsettings.discardSettings.prompt "הגדרת גשרי Tor או הכנסת הגדרות ייפוי־כוח מקומי.&#160; כדי לעשות חיבור ישיר לרשת Tor, הגדרות אלו חייבות להיות מוסרות.">
<!ENTITY torsettings.discardSettings.proceed "הסר הגדרות והתחבר">

<!ENTITY torsettings.optional "רשותי">

<!ENTITY torsettings.useProxy.checkbox "אני משתמש בייפוי־כוח כדי להתחבר לאינטרנט">
<!ENTITY torsettings.useProxy.type "סוג ייפוי־כוח">
<!ENTITY torsettings.useProxy.type.placeholder "בחר סוג ייפוי־כוח">
<!ENTITY torsettings.useProxy.address "כתובת">
<!ENTITY torsettings.useProxy.address.placeholder "כתובת IP או שם מארח">
<!ENTITY torsettings.useProxy.port "פתחה">
<!ENTITY torsettings.useProxy.username "שם משתמש">
<!ENTITY torsettings.useProxy.password "סיסמה">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "מחשב זה עובר דרך חומת אש המתירה חיבורים רק אל פתחות מסוימות">
<!ENTITY torsettings.firewall.allowedPorts "פתחות מותרות">
<!ENTITY torsettings.useBridges.checkbox "Tor מצונזר במדינה שלי">
<!ENTITY torsettings.useBridges.default "בחר גשר מובנה">
<!ENTITY torsettings.useBridges.default.placeholder "בחר גשר">
<!ENTITY torsettings.useBridges.bridgeDB "בקש גשר מן torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "הכנס את התווים מהתמונה">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "קבל אתגר חדש">
<!ENTITY torsettings.useBridges.captchaSubmit "הגש">
<!ENTITY torsettings.useBridges.custom "סַפֵּק גשר שאני מכיר">
<!ENTITY torsettings.useBridges.label "הכנס מידע גשר ממקור מהימן.">
<!ENTITY torsettings.useBridges.placeholder "הכנס כתובת:פתחה (אחד לשורה)">

<!ENTITY torsettings.copyLog "העתק את יומן Tor ללוח החיתוך">

<!ENTITY torsettings.proxyHelpTitle "עזרת ייפוי־כוח">
<!ENTITY torsettings.proxyHelp1 "ייפוי־כוח מקומי נדרש בזמן התחברות דרך רשת של חברה, בית ספר או אוניברסיטה.&#160;אם אינך בטוח כיצד לענות על שאלה זו, הבט בהגדרות האינטרנט בדפדפן אחר או בדוק בהגדרות הרשת של מערכתך.">

<!ENTITY torsettings.bridgeHelpTitle "עזרת ממסרי גשר">
<!ENTITY torsettings.bridgeHelp1 "גשרים הם ממסרים לא ברשימה אשר מקשים לחסום חיבורים אל רשת Tor.&#160; כל סוג של גשר משתמש בשיטה שונה כדי למנוע צינזור&#160; ה-obfs גורם לתעבורה שלך להיראות כרעש אקראי, וה-meek גורם לתעבורה שלך להיראות כאילו היא מתחברת אל שירות זה במקום אל Tor.">
<!ENTITY torsettings.bridgeHelp2 "מפאת כיצד מדינות מסוימות מנסות לחסום את Tor, גשרים מסוימים עובדים במדינות מסוימות אבל לא באחרות.&#160; אם אינך בטוח לגבי אילו גשרים עובדים במדינתך, בקר ב-torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "אנא המתן בזמן שאנחנו מקימים חיבור אל רשת Tor.&#160; זה עשוי לקחת מספר דקות.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "חיבור">
<!ENTITY torPreferences.torSettings "הגדרות Tor">
<!ENTITY torPreferences.torSettingsDescription "דפדפן Tor מנתב את התעבורה שלך על רשת Tor, המופעלת ע״י אלפי מתנדבים ברחבי העולם." >
<!ENTITY torPreferences.learnMore "למד עוד">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "אינטרנט:">
<!ENTITY torPreferences.statusInternetTest "בחן">
<!ENTITY torPreferences.statusInternetOnline "מחובר">
<!ENTITY torPreferences.statusInternetOffline "לא מקוון">
<!ENTITY torPreferences.statusTorLabel "רשת Tor:">
<!ENTITY torPreferences.statusTorConnected "מחובר">
<!ENTITY torPreferences.statusTorNotConnected "לא מחובר">
<!ENTITY torPreferences.statusTorBlocked "חסום פוטנציאלית">
<!ENTITY torPreferences.learnMore "למד עוד">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "התחלה זריזה">
<!ENTITY torPreferences.quickstartDescriptionLong "התחלה זריזה מחברת את דפדפן Tor אל רשת Tor באופן אוטומטי כאשר הוא מופעל, על סמך הגדרות החיבור האחרונות שהיו בשימוש.">
<!ENTITY torPreferences.quickstartCheckbox "התחבר תמיד באופן אוטומטי">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "גשרים">
<!ENTITY torPreferences.bridgesDescription "גשרים עוזרים לך להשיג גישה אל רשת Tor במקומות בהם Tor חסום. על סמך המקום שאתה נמצא בו, גשר אחד עשוי לעבוד בצורה טובה יותר מאחד אחר.">
<!ENTITY torPreferences.bridgeLocation "המיקום שלך">
<!ENTITY torPreferences.bridgeLocationAutomatic "אוטומטי">
<!ENTITY torPreferences.bridgeLocationFrequent "מיקומים נבחרים שכיחים">
<!ENTITY torPreferences.bridgeLocationOther "מיקומים אחרים">
<!ENTITY torPreferences.bridgeChooseForMe "בחר גשר עבורי…">
<!ENTITY torPreferences.bridgeBadgeCurrent "הגשרים הנוכחיים שלך">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can save one or more bridges, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "גשר #1: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "הסר">
<!ENTITY torPreferences.bridgeDisableBuiltIn "השבת גשרים מובנים">
<!ENTITY torPreferences.bridgeShare "שתף את הגשר הזה ע״י שימוש בקוד QR או ע״י העתקת הכתובת שלו:">
<!ENTITY torPreferences.bridgeCopy "העתק כתובת גשר">
<!ENTITY torPreferences.copied "הועתק!">
<!ENTITY torPreferences.bridgeShowAll "הראה את כל הגשרים">
<!ENTITY torPreferences.bridgeRemoveAll "הסר את כל הגשרים">
<!ENTITY torPreferences.bridgeAdd "הוסף גשר חדש">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "בחר באחד מהגשרים המובנים של דפדפן Tor">
<!ENTITY torPreferences.bridgeSelectBuiltin "בחר גשר מובנה…">
<!ENTITY torPreferences.bridgeRequest "בקש גשר…">
<!ENTITY torPreferences.bridgeEnterKnown "הכנס כתובת גשר שאתה כבר מכיר">
<!ENTITY torPreferences.bridgeAddManually "הוסף גשר באופן ידני…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "מתקדם">
<!ENTITY torPreferences.advancedDescription "תצר כיצד דפדפן Tor מתחבר אל האינטרנט">
<!ENTITY torPreferences.advancedButton "הגדרות…">
<!ENTITY torPreferences.viewTorLogs "הצג את יומני Tor">
<!ENTITY torPreferences.viewLogs "הצג יומנים…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "להסיר את כל הגשרים?">
<!ENTITY torPreferences.removeBridgesWarning "פעולה זו אינה ניתנת לביטול.">
<!ENTITY torPreferences.cancel "בטל">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "סרוק קוד QR">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "גשרים מובנים">
<!ENTITY torPreferences.builtinBridgeDescription "דפדפן Tor כולל כמה סוגים מסוימים של גשרים שידועים בתור &quot;תחבורות נתיקות&quot;.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 הוא סוג של גשה מובנה שגורם אל תעבורת Tor להיראות אקראית. הוא גם פחות סביר להיחסם מאשר קודמיו, גשרי obfs3.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake הוא גשר מובנה שמביס צנזורה על ידי ניתוב החיבור שלך דרך ייפויי כוח של Snowflake שמורצים על ידי מתנדבים.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure הוא גשר מובנה שגורם להיראות כאילו אתה משתמש באתר Microsoft במקום ב־Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "בקש גשר">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "יוצר קשר עם BridgeDB. אנא המתן.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "פתור את ה־CAPTCHA כדי לבקש גשר.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "הפתרון אינו נכון. אנא נסה שוב.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "ספק גשר">
<!ENTITY torPreferences.provideBridgeHeader "הכנס מידע גשר ממקור מהימן">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "הגדרות חיבור">
<!ENTITY torPreferences.connectionSettingsDialogHeader "תצר כיצד דפדפן Tor מתחבר אל האינטרנט">
<!ENTITY torPreferences.firewallPortsPlaceholder "ערכים מופרדים בפסיק">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "יומני Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "לא מחובר">
<!ENTITY torConnect.connectingConcise "מתחבר…">
<!ENTITY torConnect.tryingAgain "מנסה שוב…">
<!ENTITY torConnect.noInternet "דפדפן Tor לא היה יכול להשיג את האינטרנט">
<!ENTITY torConnect.couldNotConnect "דפדפן Tor לא היה יכול להתחבר אל Tor">
<!ENTITY torConnect.assistDescriptionConfigure "תצר את החיבור שלך"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "אם Tor חסום במיקום שלך, ניסיון גשר עשוי לעזור. סיוע בחיבור יכול לבחור אחד עבורך ע״י שימוש במיקום שלך, או שאתה יכול #1 באופן ידני במקום."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "מנסה גשר…">
<!ENTITY torConnect.tryingBridgeAgain "מנסה עוד פעם…">
<!ENTITY torConnect.errorLocation "דפדפן Tor לא היה יכול לאתר אותך">
<!ENTITY torConnect.errorLocationDescription "דפדפן Tor צריך לדעת את המיקום שלך על מנת לבחור את הגשר הנכון עבורך. אם אתה מעדיף לא לשתף את המיקום שלך, #1 באופן ידני במקום."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "האם הגדרות המיקום האלו נכונות?">
<!ENTITY torConnect.isLocationCorrectDescription "דפדפן Tor עדין לא יכול להתחבר אל Tor. אנא בדוק שהגדרות המיקום שלך נכונות ונסה שוב, או #1 במקום."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "סיוע בחיבור">
<!ENTITY torConnect.breadcrumbLocation "הגדרות מיקום">
<!ENTITY torConnect.breadcrumbTryBridge "נסה גשר">
<!ENTITY torConnect.automatic "אוטומטי">
<!ENTITY torConnect.selectCountryRegion "בחר מדינה או אזור">
<!ENTITY torConnect.frequentLocations "מיקומים נבחרים שכיחים">
<!ENTITY torConnect.otherLocations "מיקומים אחרים">
<!ENTITY torConnect.restartTorBrowser "הפעל מחדש את דפדפן Tor">
<!ENTITY torConnect.configureConnection "תצר חיבור…">
<!ENTITY torConnect.viewLog "הצג יומנים…">
<!ENTITY torConnect.tryAgain "ניסיון חוזר">
<!ENTITY torConnect.offline "אינטרנט בלתי נגיש">
<!ENTITY torConnect.connectMessage "שינויים אל הגדרות Tor לא ייכנסו לתוקף עד שתתחבר">
<!ENTITY torConnect.tryAgainMessage "דפדפן Tor נכשל בהקמת חיבור אל רשת Tor">
<!ENTITY torConnect.yourLocation "המיקום שלך">
<!ENTITY torConnect.tryBridge "נסה גשר">
<!ENTITY torConnect.autoBootstrappingFailed "תצורה אוטומטית נכשלה">
<!ENTITY torConnect.autoBootstrappingFailed "תצורה אוטומטית נכשלה">
<!ENTITY torConnect.cannotDetermineCountry "לא היה ניתן לקבוע מדינת משתמש">
<!ENTITY torConnect.noSettingsForCountry "אין הגדרות זמינות עבור המיקום שלך">
