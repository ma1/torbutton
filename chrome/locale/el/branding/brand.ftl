# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Περιηγητής Tor
-brand-short-name = Περιηγητής Tor
-brand-full-name = Περιηγητής Tor
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Περιηγητής Tor
-vendor-short-name = Περιηγητής Tor
trademarkInfo = Το 'Tor' και το 'Onion Logo' είναι κατατεθέντα σήματα του Tor Project, Inc.
