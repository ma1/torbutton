<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Acerca de Tor">

<!ENTITY aboutTor.viewChangelog.label "Ver registro de cambios">

<!ENTITY aboutTor.ready.label "Explorá. Privadamente.">
<!ENTITY aboutTor.ready2.label "Estás listo para la experiencia de navegación más privada del mundo.">
<!ENTITY aboutTor.failure.label "¡Algo salió mal!">
<!ENTITY aboutTor.failure2.label "Tor no funciona en este navegador.">

<!ENTITY aboutTor.search.label "Buscá con DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "¿Preguntas?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Revisá nuestro manual del Navegador Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Manual del Navegador Tor">

<!ENTITY aboutTor.tor_mission.label "El proyecto Tor es una organización sin fines de lucro bajo las provisiones de la ley EUA 501(c)(3), cuya misión es avanzar los derechos y libertades humanas creando y desplegando tecnologías de anonimato y privacidad de fuente abierta, soportando su disponibilidad y uso irrestricto, y ampliando su entendimiento científico y popular.">
<!ENTITY aboutTor.getInvolved.label "Involucrate »">

<!ENTITY aboutTor.newsletter.tagline "Recibí las últimas noticias de Tor derecho en tu bandeja de entrada.">
<!ENTITY aboutTor.newsletter.link_text "Registrate en Tor News.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor puede ser usado libremente gracias a las donaciones de personas como vos.">
<!ENTITY aboutTor.donationBanner.buttonA "Doná ahora">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "DONÁ AHORA">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Tu donación va a ser emparejada por Friends of Tor, hasta USD 100.000.">
