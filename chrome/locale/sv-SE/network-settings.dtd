<!ENTITY torsettings.dialog.title "Nätverksinställningar för Tor">
<!ENTITY torsettings.wizard.title.default "Anslut till Tor">
<!ENTITY torsettings.wizard.title.configure "Nätverksinställningar för Tor">
<!ENTITY torsettings.wizard.title.connecting "Etablerar en anslutning">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Språk för Tor Browser">
<!ENTITY torlauncher.localePicker.prompt "Välj ett språk.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Klicka på &quot;Anslut&quot; för att ansluta till Tor.">
<!ENTITY torSettings.configurePrompt "Klicka på &quot;Konfigurera&quot; för att justera nätverksinställningar om du är i ett land som censurerar Tor (som Egypten, Kina eller Turkiet) eller om du ansluter från ett privat nätverk som kräver en proxy.">
<!ENTITY torSettings.configure "Konfigurera">
<!ENTITY torSettings.connect "Anslut">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Väntar på att Tor ska starta...">
<!ENTITY torsettings.restartTor "Starta om Tor">
<!ENTITY torsettings.reconfigTor "Omkonfigurera">

<!ENTITY torsettings.discardSettings.prompt "Du har konfigurerat Tor-broar eller så har du angett lokala proxyinställningar.&#160; För att göra en direktanslutning till Tor-nätverket måste de här inställningarna tas bort.">
<!ENTITY torsettings.discardSettings.proceed "Ta bort inställningar och anslut">

<!ENTITY torsettings.optional "Valfritt">

<!ENTITY torsettings.useProxy.checkbox "Jag använder en proxy för att ansluta till internet">
<!ENTITY torsettings.useProxy.type "Proxytyp">
<!ENTITY torsettings.useProxy.type.placeholder "Välj en proxytyp">
<!ENTITY torsettings.useProxy.address "Adress">
<!ENTITY torsettings.useProxy.address.placeholder "IP-adress eller värdnamn">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Användarnamn">
<!ENTITY torsettings.useProxy.password "Lösenord">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Denna dator går genom en brandvägg som bara tillåter anslutningar på vissa specifika portar.">
<!ENTITY torsettings.firewall.allowedPorts "Tillåtna portar">
<!ENTITY torsettings.useBridges.checkbox "Tor är censurerat i mitt land">
<!ENTITY torsettings.useBridges.default "Välj en inbyggd bro">
<!ENTITY torsettings.useBridges.default.placeholder "Välj en bro">
<!ENTITY torsettings.useBridges.bridgeDB "Begär en bro från torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Ange tecknen från bilden">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Få en ny utmaning">
<!ENTITY torsettings.useBridges.captchaSubmit "Skicka">
<!ENTITY torsettings.useBridges.custom "Ange en bro jag känner till">
<!ENTITY torsettings.useBridges.label "Ange broinformationen från en betrodd källa">
<!ENTITY torsettings.useBridges.placeholder "skriv adress:port (en per rad)">

<!ENTITY torsettings.copyLog "Kopiera Tor-logg till urklipp">

<!ENTITY torsettings.proxyHelpTitle "Proxyhjälp">
<!ENTITY torsettings.proxyHelp1 "En lokal proxy kan behövas när du ansluter via ett företags-, skol- eller universitetsnätverk.&#160;Om du inte är säker på om en proxy behövs, titta på internetinställningarna i en annan webbläsare eller kontrollera systemets nätverksinställningar.">

<!ENTITY torsettings.bridgeHelpTitle "Hjälp om relä-broar">
<!ENTITY torsettings.bridgeHelp1 "Broar är olistade reläer som gör det svårare att blockera anslutningar till Tor-nätverket. &#160; Var typ av bro använder sig av olika metoder för att undvika censur. &#160; Obfs gör att din trafik ser ut som brus och meek gör att din trafik ser ut som att du ansluter till tjänsten istället för Tor.">
<!ENTITY torsettings.bridgeHelp2 "I och med att vissa länder försöker blockera Tor fungerar inte alla broar i alla länder. &#160; Ifall du är osäker på vilka broar som fungerar i ditt land besök torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Vänta medan vi etablerar en anslutning till Tor-nätverket. &#160; Detta kan ta flera minuter.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Anslutning">
<!ENTITY torPreferences.torSettings "Tor-inställningar">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser dirigerar din trafik över Tor-nätverket, som drivs av tusentals volontärer runt om i världen." >
<!ENTITY torPreferences.learnMore "Läs mer">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Testa">
<!ENTITY torPreferences.statusInternetOnline "Uppkopplad">
<!ENTITY torPreferences.statusInternetOffline "Nedkopplad">
<!ENTITY torPreferences.statusTorLabel "Tor-nätverk:">
<!ENTITY torPreferences.statusTorConnected "Ansluten">
<!ENTITY torPreferences.statusTorNotConnected "Inte ansluten">
<!ENTITY torPreferences.statusTorBlocked "Potentiellt blockerad">
<!ENTITY torPreferences.learnMore "Läs mer">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Snabbstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Snabbstart ansluter Tor Browser till Tor-nätverket automatiskt när den startas, baserat på dina senast använda anslutningsinställningar.">
<!ENTITY torPreferences.quickstartCheckbox "Anslut alltid automatiskt">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Broar">
<!ENTITY torPreferences.bridgesDescription "Broar hjälper dig att komma åt Tor-nätverket på platser där Tor är blockerat. Beroende på var du är, kan en bro fungera bättre än en annan.">
<!ENTITY torPreferences.bridgeLocation "Din plats">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatisk">
<!ENTITY torPreferences.bridgeLocationFrequent "Ofta valda platser">
<!ENTITY torPreferences.bridgeLocationOther "Andra platser">
<!ENTITY torPreferences.bridgeChooseForMe "Välj en bro för mig...">
<!ENTITY torPreferences.bridgeBadgeCurrent "Dina nuvarande broar">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "Du kan spara en eller flera broar och Tor väljer vilken som ska användas när du ansluter. Tor kommer automatiskt att byta till att använda en annan bro när det behövs.">
<!ENTITY torPreferences.bridgeId "#1 bro: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Ta bort">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Inaktivera inbyggda broar">
<!ENTITY torPreferences.bridgeShare "Dela den här bron med QR-koden eller genom att kopiera dess adress:">
<!ENTITY torPreferences.bridgeCopy "Kopiera broadress">
<!ENTITY torPreferences.copied "Kopierat!">
<!ENTITY torPreferences.bridgeShowAll "Visa alla broar">
<!ENTITY torPreferences.bridgeRemoveAll "Ta bort alla broar">
<!ENTITY torPreferences.bridgeAdd "Lägg till en ny bro">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Välj från en av Tor Browsers inbyggda broar">
<!ENTITY torPreferences.bridgeSelectBuiltin "Välj en inbyggd bro...">
<!ENTITY torPreferences.bridgeRequest "Begär en bro...">
<!ENTITY torPreferences.bridgeEnterKnown "Ange en broadress som du redan känner till">
<!ENTITY torPreferences.bridgeAddManually "Lägg till en bro manuellt...">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Avancerat">
<!ENTITY torPreferences.advancedDescription "Konfigurera hur Tor Browser ansluter till internet">
<!ENTITY torPreferences.advancedButton "Inställningar…">
<!ENTITY torPreferences.viewTorLogs "Se Tor-loggarna">
<!ENTITY torPreferences.viewLogs "Visa loggar…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Ta bort alla broar?">
<!ENTITY torPreferences.removeBridgesWarning "Denna åtgärd kan inte ångras.">
<!ENTITY torPreferences.cancel "Avbryt">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Skanna QR-koden">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Inbyggda broar">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser innehåller några specifika typer av broar som kallas &quot;pluggbara transporter&quot;.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 är en typ av inbyggd bro som får din Tor-trafik att se slumpmässig ut. De är också mindre benägna att blockeras än sina föregångare, obfs3-broar.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake är en inbyggd bro som besegrar censur genom att dirigera din anslutning genom Snowflake-proxyer, som drivs av frivilliga.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure är en inbyggd bro som får det att se ut som att du använder en Microsoft-webbplats istället för att använda Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Begär bro">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Kontaktar BridgeDB. Vänta.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Lös CAPTCHA för att begära en bro.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Lösningen är inte korrekt. Försök igen.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Tillhandahåll bro">
<!ENTITY torPreferences.provideBridgeHeader "Ange broinformation från en betrodd källa">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Anslutningsinställningar">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Konfigurera hur Tor Browser ansluter till internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Kommaseparerade värden">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor-loggar">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Inte ansluten">
<!ENTITY torConnect.connectingConcise "Ansluter...">
<!ENTITY torConnect.tryingAgain "Försöker igen...">
<!ENTITY torConnect.noInternet "Tor Browser kunde inte nå internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser kunde inte ansluta till Tor">
<!ENTITY torConnect.assistDescriptionConfigure "konfigurera din anslutning"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Om Tor är blockerad på din plats kan det hjälpa att pröva en bro. Anslutningshjälp kan välja en åt dig med din plats, eller så kan du #1 manuellt istället."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Prövar en bro...">
<!ENTITY torConnect.tryingBridgeAgain "Försöker en gång till...">
<!ENTITY torConnect.errorLocation "Tor Browser kunde inte hitta din plats">
<!ENTITY torConnect.errorLocationDescription "Tor Browser behöver veta din plats för att kunna välja rätt bro för dig. Om du hellre inte vill dela din plats, #1 manuellt istället."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Är dessa platsinställningar korrekta?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser kunde fortfarande inte ansluta till Tor. Kontrollera att dina platsinställningar är korrekta och försök igen, eller #1 istället."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Anslutningshjälp">
<!ENTITY torConnect.breadcrumbLocation "Platsinställningar">
<!ENTITY torConnect.breadcrumbTryBridge "Pröva en bro">
<!ENTITY torConnect.automatic "Automatisk">
<!ENTITY torConnect.selectCountryRegion "Välj land eller region">
<!ENTITY torConnect.frequentLocations "Ofta valda platser">
<!ENTITY torConnect.otherLocations "Andra platser">
<!ENTITY torConnect.restartTorBrowser "Starta om Tor Browser">
<!ENTITY torConnect.configureConnection "Konfigurera anslutning...">
<!ENTITY torConnect.viewLog "Visa loggar...">
<!ENTITY torConnect.tryAgain "Försök igen">
<!ENTITY torConnect.offline "Internet är onåbart">
<!ENTITY torConnect.connectMessage "Ändringar av Tor-inställningar träder inte i kraft förrän du ansluter">
<!ENTITY torConnect.tryAgainMessage "Tor Browser har misslyckats med att upprätta en anslutning till Tor-nätverket">
<!ENTITY torConnect.yourLocation "Din plats">
<!ENTITY torConnect.tryBridge "Pröva en bro">
<!ENTITY torConnect.autoBootstrappingFailed "Automatisk konfiguration misslyckades">
<!ENTITY torConnect.autoBootstrappingFailed "Automatisk konfiguration misslyckades">
<!ENTITY torConnect.cannotDetermineCountry "Det går inte att bestämma användarland">
<!ENTITY torConnect.noSettingsForCountry "Inga inställningar tillgängliga för din plats">
