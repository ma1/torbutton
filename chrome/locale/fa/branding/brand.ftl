# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = مرورگر Tor
-brand-short-name = مرورگر Tor
-brand-full-name = مرورگر Tor
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = مرورگر تور
-vendor-short-name = پروژه Tor
trademarkInfo = 'Tor' و 'Onion Logo' علامت های تجاری ثبت شده از پروژه Tor هستند
